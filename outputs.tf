output "instance_id" {
  description = "The ID of the instance"
  value       = openstack_compute_instance_v2.compute_instance.id
}
output "port_id" {
  description = "The IDs of the instance ports"
  #  value       = [for p in openstack_networking_port_v2.port : p.name]
  #  value       = openstack_compute_instance_v2.compute_instance.network["uuid"]
  value = [for item in openstack_compute_instance_v2.compute_instance.network : item.port]
}
