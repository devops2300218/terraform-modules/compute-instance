variable "region" {
  description = "region of the compute instance"
  type        = string
}

variable "instance_name" {
  description = "name of the compute instance"
  type        = string
}

variable "image_name" {
  description = "image to use for the compute instance"
  type        = string
}

variable "instance_flavor" {
  description = "flavor to use for the compute instance"
  type        = string
}

variable "keypair" {
  description = "keypair to use for initial access"
  type        = string
}

variable "private_ips" {
  description = "map of private ips to attach to the instance"
  type = list(object({
    private_ip = string,
    network_id = string,
    subnet_id  = string
  }))
}

variable "volumes" {
  description = "list of volume sizes"
  type = list(object({
    size        = number,
    volume_type = string
  }))
}
