resource "openstack_networking_port_v2" "port" {
  count = length(var.private_ips)

  region     = var.region
  network_id = var.private_ips[count.index].network_id

  fixed_ip {
    ip_address = var.private_ips[count.index].private_ip
    subnet_id  = var.private_ips[count.index].subnet_id
  }
}

resource "openstack_compute_instance_v2" "compute_instance" {
  region      = var.region
  name        = var.instance_name
  image_name  = var.image_name
  flavor_name = var.instance_flavor
  key_pair    = var.keypair

  dynamic "network" {
    for_each = range(length(var.private_ips))

    content {
      port = openstack_networking_port_v2.port[network.value].id
    }
  }
}

resource "openstack_blockstorage_volume_v3" "volume" {
  count = length(var.volumes)

  region      = var.region
  size        = var.volumes[count.index].size
  name        = "${var.instance_name}_volume_${count.index}"
  volume_type = var.volumes[count.index].volume_type
}

resource "openstack_compute_volume_attach_v2" "volume_attach" {
  count = length(var.volumes)

  region      = var.region
  instance_id = openstack_compute_instance_v2.compute_instance.id
  volume_id   = openstack_blockstorage_volume_v3.volume[count.index].id
}
